const express = require("express");

// mongoose is a package that allows creating of Schemas to our model our data structures
// also has access to a number methods for manipulating our database;
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://richrddoma:admin123@zuitt-bootcamp.xpydmcq.mongodb.net/s35?retryWrites=true&w=majority",
	{	
		// allows us to avoid any current and future errors while connecting to mongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);

// allows to handle errors when the initial connection is established
let db = mongoose.connection;

// console.error.bind(console) allows us to print error in the browser console and in the terminal
// "connection error" is the mssge that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));
// if the connection is successful, out in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Connecting to MongoDB Atlas END

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose schemas

const taskSchema = new mongoose.Schema({
	name: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
});

// [SECTIOM] Mongoose Models
// Models must be in singular form and capitalize
const Task = mongoose.model("Task", taskSchema);

// Creation of Task Application
//  Create a new task
/*
BUSINESS LOGIC
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
3. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name}).then((result, err) => {

		if(result != null && result.name == req.body.name){
			return res.send('Duplicate task found');
		} else {
			let newTask = new Task({
				name: req.body.name,
				password: req.body.password
			});

			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New task created!");
				}
			})
		}

	})
});


// Getting all the task

/*
BUSINESS LOGIC
1.Retrieve all the documents
2.If an error is encountered, print the error
3.If no errors are found, send a success status back to the client/Postman and return an array of documents
*/


app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json ({
				data: result
			})
		}
	})
}); 





// ====== ACTIVITY ======

const userSchema = new mongoose.Schema({
	username: String,
	password: String,

});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
	User.findOne({ username: req.body.name}).then((result, err) => {

		if(result != null && result.username == req.body.username){
			return res.send('Username has already use');
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save().then((savedUser, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user created!");
				}
			})
		}

	})
});



app.get("/signup", (req, res) => {
	User.find({}).then((result, err) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json ({
				data: result
			})
		}
	})
}); 





app.listen(port, () => console.log(`Server running at port ${port}`));
